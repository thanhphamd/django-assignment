import operator
from django.views import generic
from django.db.models import Q
from functools import reduce
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


class FilteredListView(generic.ListView):
    """Filters the queryset by a search term given in a GET argument."""

    def get_search_term(self):
        """Return the search term by looking under the '?q=' GET argument."""
        return self.request.GET.get('q')

    def paginate(self, queryset):
        paginator = Paginator(queryset, self.paginate_number)

        page = self.request.GET.get('page')
        try:
            queryset = paginator.page(page)
        except PageNotAnInteger:
            queryset = paginator.page(1)
        except EmptyPage:
            queryset = paginator.page(paginator.num_pages)
        return queryset

    def get_queryset(self):
        """
        Return the filtered queryset.

        Search through get_unfiltered_queryset for occurrences of get_search_term in
        any of self.search_fields.  If search_fields is empty or missing, or
        there is no search term, simply return the unfiltered queryset.
        """

        base_queryset = super().get_queryset()
        search_fields = set(self.search_fields)
        search_term = self.get_search_term()
        if not search_fields or not search_term:
            return self.paginate(base_queryset)

        def make_q_item(field_name, search_term):
            """Build a Q object representing [field_name]__icontains=[search_term]."""
            field_condition = field_name + '__icontains'
            return Q(**{field_condition: search_term})

        filter_conditions = [make_q_item(field, search_term) for field in search_fields]
        filter_operation = reduce(operator.or_, filter_conditions)
        return self.paginate(base_queryset.filter(filter_operation))
