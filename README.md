Step:
//Create and activate a (python) virtual environment
sudo apt-get update
sudo apt-get install python3-pip
sudo pip3 install virtualenv

mkdir ~/virtualenv
cd ~/virtualenv
virtualenv newenv
source newenv/bin/activate


// Install django 
pip install django 

// Before install Pillow
sudo apt-get install libjpeg-dev

//Install PostgreSQL database, create a database and create a superuser

sudo apt-get install postgresql postgresql-contrib
sudo -i -u postgres
createdb test_database
createuser --interactive
Enter name of role to add : thanhphamd1
Shall the new role be a superuser? (y/n) y

psql
alter user thanhphamd1 with password '123456';
CTRL+D


//Install driver for PostgreSQl
sudo apt-get install libpq-dev
pip install psycopg2


// Create a new project and add database setting to settings.py
django-admin.py startproject myproject
Open myproject/myproject/settings.py
Add 
DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2', 
            'NAME': 'test_database',                     
            'USER': 'thanhphamd1',
            'PASSWORD': '123456',
            'HOST': 'localhost',                      
            'PORT': '',                     
        }
    }



//Run project
cd myproject
python manage.py syncdb
python manage.py migrate

python manage.py runserver

//Install Ruby
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev


//Support deployment using Farbic
pip install fabric

//Install git
sudo apt-get update
sudo apt-get install git

//Install Ruby
sudo apt-get install git-core curl zlib1g-dev build-essential libssl-dev libreadline-dev libyaml-dev libsqlite3-dev sqlite3 libxml2-dev libxslt1-dev libcurl4-openssl-dev python-software-properties libffi-dev

cd
git clone git://github.com/sstephenson/rbenv.git .rbenv
echo 'export PATH="$HOME/.rbenv/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(rbenv init -)"' >> ~/.bashrc
exec $SHELL

git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
echo 'export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' >> ~/.bashrc
exec $SHELL

git clone https://github.com/sstephenson/rbenv-gem-rehash.git ~/.rbenv/plugins/rbenv-gem-rehash

rbenv install 2.2.3
rbenv global 2.2.3
ruby -v


//Deploy
fab deploy