module.exports = function (grunt) {

    'use strict';

    if (grunt.option('help')) {
        // Load all tasks so they can be viewed in the help: grunt -h or --help.
        require('load-grunt-tasks')(grunt);
    } else {
        // Use jit-grunt to only load necessary tasks for each invocation of grunt.
        require('jit-grunt')(grunt);
    }

    require('time-grunt')(grunt);

    grunt.initConfig({

        config: {
            baseDir: 'mysite/',
            staticDir: '<%= config.baseDir %>/static/',
            libDir: '<%= config.staticDir %>/lib/',
            cssDir: '<%= config.staticDir %>/styles/',
            sassDir: '<%= config.baseDir %>/sass',
        },

        compass: {
            options: {
                sassDir: '<%= config.sassDir %>',
                cssDir: '<%= config.cssDir %>',
                relativeAssets: true,
                outputStyle: 'expanded',
                debugInfo: false,
            },
            dev: {
                // Options object is required.
                options: {}
            },
            force: {
                options: {
                    force: true
                }
            },
            watch: {
                options: {
                    watch: true
                }
            },
            dist: {
                options: {
                    // Force to be certain the compile is correct.
                    force: true,
                    outputStyle: 'compressed',
                    noLineComments: true
                }
            }
        },


        concurrent: {
            options: {
                logConcurrentOutput: true
            },
            dev: [
                'compass:watch'
            ],
            dist: [
                'compass:dist'
            ]
        },


    });

    grunt.registerTask('default', ['dev']);

    grunt.registerTask(
        'dev',
        'Compile Sass and watch for changes',
        // First compile so styles are available, then watch.
        ['compass:dev', 'concurrent:dev']
    );

    grunt.registerTask(
        'dist',
        'Build for deploy',
        ['concurrent:dist']
    );

};