from django.db import models
from django.contrib.auth.models import User


class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    job_title = models.CharField(max_length=100, default='Unemployed', null=True)
    region = models.CharField(max_length=100, default='', null=True)
    interest = models.CharField(max_length=100, default='', null=True)
    specialism = models.CharField(max_length=100, default='', null=True)
    avatar = models.ImageField(upload_to='user_avatar', null=True)
