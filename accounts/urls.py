from django.conf.urls import url, patterns

from . import views


urlpatterns = patterns(
    "",
    url(r'^$', views.UserList.as_view(), name='list_user'),
    url(r'^login/$', views.login, name='login'),
    url(r'^logout/$', views.logout, name='logout'),
    url(r'^(?P<pk>[0-9]+)/$', views.DetailUserView.as_view(), name='detail'),
    url(r'^getname/$', views.get_name, name='get_name'),
    url(r'^register/$', views.register, name='register'),
)
