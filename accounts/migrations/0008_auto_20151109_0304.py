# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0007_remove_account_full_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='interest',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='account',
            name='region',
            field=models.CharField(default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='account',
            name='specialism',
            field=models.CharField(default='', max_length=100),
        ),
    ]
