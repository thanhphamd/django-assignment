# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20151105_1054'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='is_hungry',
            field=models.BooleanField(default=True),
        ),
    ]
