# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_account_is_hungry'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='age',
            field=models.IntegerField(default=10),
        ),
    ]
