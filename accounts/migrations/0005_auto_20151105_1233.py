# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_account_age'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='account',
            name='age',
        ),
        migrations.RemoveField(
            model_name='account',
            name='is_hungry',
        ),
    ]
