# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='account',
            name='department',
        ),
        migrations.AddField(
            model_name='account',
            name='interest',
            field=models.CharField(default='Nothing', max_length=100),
        ),
        migrations.AddField(
            model_name='account',
            name='job_title',
            field=models.CharField(default='Unemployed', max_length=100),
        ),
        migrations.AddField(
            model_name='account',
            name='region',
            field=models.CharField(default='Atheistism', max_length=100),
        ),
        migrations.AddField(
            model_name='account',
            name='specialism',
            field=models.CharField(default='Nothing', max_length=100),
        ),
    ]
