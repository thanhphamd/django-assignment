# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0006_account_full_name'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='account',
            name='full_name',
        ),
    ]
