# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0011_auto_20151109_0708'),
    ]

    operations = [
        migrations.AlterField(
            model_name='account',
            name='interest',
            field=models.CharField(null=True, default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='account',
            name='job_title',
            field=models.CharField(null=True, default='Unemployed', max_length=100),
        ),
        migrations.AlterField(
            model_name='account',
            name='region',
            field=models.CharField(null=True, default='', max_length=100),
        ),
        migrations.AlterField(
            model_name='account',
            name='specialism',
            field=models.CharField(null=True, default='', max_length=100),
        ),
    ]
