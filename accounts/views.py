from django.shortcuts import render
from django.contrib import auth
from django.core.context_processors import csrf
from django.views import generic
from .models import Account
from mysite.views import FilteredListView
from django.conf import settings
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from .forms import NameForm, MyRegistrationForm


def login(request):
    if request.method == "GET":
        c = {}
        c.update(csrf(request))
        next = request.GET.get('next', '')
        next = next[:-1]
        c.update({'next': next})
        return render(request, 'accounts/login.html', c)
    if request.method == "POST":
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")
        remember = request.POST.get("remember", "no")
        next = request.POST.get('next', '/')
        if (username == "" or password == ""):
            error_message = "Please type in both username and password."
            return render(
                request, 'accounts/login.html', {'error_message': error_message}
                )
        else:
            user = auth.authenticate(username=username, password=password)

            if user is not None:
                auth.login(request, user)
                if remember == "yes":
                    settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = False
                else:
                    settings.SESSION_EXPIRE_AT_BROWSER_CLOSE = True
                return HttpResponseRedirect(next)
            else:
                next = next[:-1]
                error_message = "Your loggin is invalid. Please try again."
                return render(
                    request, 'accounts/login.html',
                    {'error_message': error_message, 'next': next}
                    )


def logout(request):
    auth.logout(request)
    return render(request, 'accounts/logout.html')


class DetailUserView(generic.DetailView):
    template_name = 'accounts/detail.html'
    model = Account


class UserList(FilteredListView):
    template_name = 'accounts/member_directory.html'
    context_object_name = 'accounts'
    paginate_number = 4
    search_fields = [
        'user__first_name', 'user__last_name', 'region', 'interest', 'specialism'
    ]

    def get_queryset(self):
        accounts = Account.objects.all().order_by('user__first_name', 'user__last_name')
        self.queryset = accounts
        return super().get_queryset()


def get_name(request):
    # if this is a POST request, we need to process the form data,
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        form = NameForm(request.POST)
        # check whether it's valid
        if form.is_valid():
            # process the data in form.cleaned_data as required
            # ...
            # redirect to a new URL:
            return HttpResponseRedirect(reverse('accounts:login'))
    # if a GET (or any other method) we'll create a blank form
    else:
        form = NameForm()

    return render(request, 'accounts/name.html', {'form': form})


def register(request):
    if request.method == 'POST':
        form = MyRegistrationForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('accounts:list_user'))

    args = {}
    args.update(csrf(request))
    args['form'] = MyRegistrationForm()
    return render(request, 'accounts/register.html', args)
