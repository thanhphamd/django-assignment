from django.test import TestCase
from django.core.urlresolvers import reverse
from .models import Account
from django.contrib.auth.models import User


class TestAccountModel(TestCase):
    def test_fields(self):
        fields = Account._meta.get_all_field_names()
        expected = {
            'specialism',
            'region',
            'job_title',
            'avatar',
            'id',
            'interest',
            'user',
            'user_id'
        }
        self.assertCountEqual(fields, expected)


def create_account(username, password):
    user = User.objects.create_user(username=username, password=password)
    return Account.objects.create(user=user)


class TestLoginView(TestCase):
    def test_login_with_an_invalid_account(self):
        response = self.client.post(reverse('accounts:login'),
                                    {'username': "username", 'password': "password"})
        self.assertContains(response, "Your loggin is invalid. Please try again.",
                            status_code=200)

    def test_login_with_a_valid_account(self):
        create_account(username='username', password='password')
        self.client.get(reverse('accounts:login'))
        response = self.client.post(reverse('accounts:login'),
                                    {"username": "username", "password": "password"})
        self.assertRedirects(response, reverse('accounts:list_user'),
                             status_code=302, target_status_code=200)

    def test_login_not_typing_username_or_password(self):
        self.client.get(reverse('accounts:login'))
        response = self.client.post(reverse('accounts:login'),
                                    {"username": "username"})
        self.assertContains(response, "Please type in both username and password.",
                            status_code=200)


class TestLogoutView(TestCase):
    def test_logout_view(self):
        response = self.client.get(reverse('accounts:logout'))
        self.assertContains(response, "Good bye!!!",
                            status_code=200)


class TestDetailUserView(TestCase):
    def test_go_to_detail_view_after_login(self):
        create_account(username="username", password="password")
        self.client.login(username="username", password="password")
        response = self.client.get(reverse('accounts:detail', args=(1,)))
        self.assertContains(response, "USER PROFILE",
                            status_code=200)

    def test_go_to_detail_view_before_login(self):
        response = self.client.get(reverse('accounts:detail', args=(1,)))
        self.assertRedirects(response, reverse('accounts:login')
                             + "?next=" + reverse('accounts:detail', args=(1,)),
                             status_code=302, target_status_code=200)


class TestUserListView(TestCase):
    def test_go_to_user_list_view_after_login(self):
        create_account(username="username", password="password")
        self.client.login(username="username", password="password")
        response = self.client.get(reverse('accounts:list_user'))
        self.assertContains(response, "Member directory",
                            status_code=200)

    def test_go_to_user_list_view_before_login(self):
        response = self.client.get(reverse('accounts:list_user'))
        self.assertRedirects(response, reverse('accounts:login')
                             + "?next=" + reverse('accounts:list_user'),
                             status_code=302, target_status_code=200)

    def test_pagination_going_to_the_last_page(self):
        """
        The template should contain a disabled 'Next' button
        and should not contain the '3' button .
        """
        create_account(username="username1", password="password")
        create_account(username="username2", password="password")
        create_account(username="username3", password="password")
        create_account(username="username4", password="password")
        create_account(username="username5", password="password")

        self.client.login(username="username1", password="password")
        response = self.client.get(reverse('accounts:list_user')+"?page=2")
        self.assertContains(response,
                            '<button class="step-buttons" type="button" disabled>Next\
                            </button>',
                            html=True, status_code=200
                            )
        self.assertContains(response, '<input class="step-buttons" onclick="location.href=\'?q=&amp;page=2\';"\
                            value="2" type="button">',
                            html=True
                            )
        self.assertNotContains(response, '<input class="step-buttons" onclick="location.href=\'?q=&amp;page=3\';"\
                               value="3" type="button">',
                               html=True
                               )

    def test_pagination_going_to_the_first_page(self):
        """
        The template should contain a disabled 'First' button
        and should not contain the '3' button .
        """
        create_account(username="username1", password="password")
        create_account(username="username2", password="password")
        create_account(username="username3", password="password")
        create_account(username="username4", password="password")
        create_account(username="username5", password="password")

        self.client.login(username="username1", password="password")
        response = self.client.get(reverse('accounts:list_user'))
        self.assertContains(response,
                            '<button class="step-buttons" type="button" disabled>Previous\
                            </button>',
                            html=True, status_code=200
                            )
        self.assertContains(response, '<input class="step-buttons" onclick="location.href=\'?q=&amp;page=1\';"\
                            value="1" type="button">',
                            html=True
                            )
        self.assertNotContains(response, '<input class="step-buttons" onclick="location.href=\'?q=&amp;page=3\';"\
                               value="3" type="button">',
                               html=True
                               )

    def test_pagination_going_to_a_not_integer_page(self):
        """
        The view should return to the first page .
        The template should contain a disable 'Previous' button .
        """
        create_account(username="username1", password="password")
        create_account(username="username2", password="password")
        create_account(username="username3", password="password")
        create_account(username="username4", password="password")
        create_account(username="username5", password="password")

        self.client.login(username="username1", password="password")
        response = self.client.get(reverse('accounts:list_user')+"?page=1.5")
        self.assertContains(response,
                            '<button class="step-buttons" type="button" disabled>Previous\
                            </button>',
                            html=True, status_code=200
                            )

    def test_pagination_going_to_an_empty_page(self):
        """
        The view should return to the last page .
        The template should contain a disable 'Next' button .
        """
        create_account(username="username1", password="password")
        create_account(username="username2", password="password")
        create_account(username="username3", password="password")
        create_account(username="username4", password="password")
        create_account(username="username5", password="password")

        self.client.login(username="username1", password="password")
        response = self.client.get(reverse('accounts:list_user')+"?page=3")
        self.assertContains(response,
                            '<button class="step-buttons" type="button" disabled>Next\
                            </button>',
                            html=True, status_code=200
                            )

    def test_search_function(self):
        account1 = create_account(username="username1", password="password")
        account1.user.first_name = "A"
        account1.user.last_name = "B"
        account1.region = "C"
        account1.interest = "D"
        account1.specialism = "E"
        account1.user.save()
        account1.save()
        account2 = create_account(username="username2", password="password")
        account2.user.first_name = "1"
        account2.user.last_name = "2"
        account2.region = "3"
        account2.interest = "4"
        account2.specialism = "5"
        account2.user.save()
        account2.save()

        self.client.login(username="username1", password="password")

        # Search by name
        response = self.client.get(reverse('accounts:list_user')+"?q=%s"
                                   % (account1.user.first_name))
        self.assertContains(response, "%s %s"
                            % (account1.user.first_name, account1.user.last_name),
                            status_code=200)
        self.assertNotContains(response, "%s %s"
                               % (account2.user.first_name, account2.user.last_name))

        # Search by region
        response = self.client.get(reverse('accounts:list_user')+"?q=%s"
                                   % (account1.region))
        self.assertContains(response, "%s %s"
                            % (account1.user.first_name, account1.user.last_name),
                            status_code=200)
        self.assertNotContains(response, "%s %s"
                               % (account2.user.first_name, account2.user.last_name))

        # Search by interests
        response = self.client.get(reverse('accounts:list_user')+"?q=%s"
                                   % (account1.interest))
        self.assertContains(response, "%s %s"
                            % (account1.user.first_name, account1.user.last_name),
                            status_code=200)
        self.assertNotContains(response, "%s %s"
                               % (account2.user.first_name, account2.user.last_name))

        # Search by specialism
        response = self.client.get(reverse('accounts:list_user')+"?q=%s"
                                   % (account1.specialism))
        self.assertContains(response, "%s %s"
                            % (account1.user.first_name, account1.user.last_name),
                            status_code=200)
        self.assertNotContains(response, "%s %s"
                               % (account2.user.first_name, account2.user.last_name))
