from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Account
from django.forms import ModelForm


class NameForm(forms.Form):
    your_name = forms.CharField(label='Your name', max_length=100)
    subject = forms.CharField(max_length=100)
    message = forms.CharField(widget=forms.Textarea)
    sender = forms.EmailField()
    cc_myself = forms.BooleanField(required=False)

class MyRegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)
    first_name = forms.CharField(max_length=50, required=True)
    last_name = forms.CharField(max_length=50, required=True)
    job_title = forms.CharField(max_length=50, required=True)
    region = forms.CharField(max_length=50, required=False)
    interest = forms.CharField(max_length=50, required=False)
    specialism = forms.CharField(max_length=50, required=False)
    avatar = forms.ImageField(required=False)

    class Meta:
        model = User
        fields = ('username',
                  'email',
                  'password1',
                  'password2',
                  'first_name',
                  'last_name',
                  'job_title',
                  'region',
                  'interest',
                  'specialism',
                  'avatar')

    def save(self, commit=True):
        user = super(MyRegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']

        if commit:
            user.save()
            account = Account.objects.create(user=user)
            account.job_title = self.cleaned_data['job_title']
            account.region = self.cleaned_data['region']
            account.interest = self.cleaned_data['interest']
            account.specialism = self.cleaned_data['specialism']
            account.avatar = self.cleaned_data['avatar']
            account.save()

        return user
