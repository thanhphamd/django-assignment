from fabric.api import cd, env, require, run, sudo, task, prefix
from fabric.contrib.files import exists

env.host_string = 'localhost'
env.user = 'thanhphamd1'
env.password = '123456'
env.site_path = '/home/thanhphamd1/newproject/deploy'


@task
def manage(command, manage='manage.py', envdir='env'):
    """Run a django management command. e.g. deploy.manage:syncdb"""
    require('site_path')
    with cd(env.site_path):
        with prefix("source /home/thanhphamd1/newproject/newenv/bin/activate "):
            cmd = 'python {0} {1}'.format(manage, command)
            run(cmd)


@task
def pip(requirements_file='requirements.txt'):
    """Run pip install."""
    require('site_path')
    with cd(env.site_path):
        if exists(requirements_file):
            with prefix("source /home/thanhphamd1/newproject/newenv/bin/activate "):
                run('pip install -r {0}'.format(requirements_file))


@task
def pull():
    """Run git pull."""
    require('site_path')
    with cd(env.site_path):
        run('git init')
        run('git pull https://thanhphamd@bitbucket.org/thanhphamd/django-assignment.git')


@task
def supervisor(command):
    """Run a supervisorctl command. e.g. deploy.supervisor:'restart all'"""
    sudo('supervisorctl {0}'.format(command), shell=False)


@task
def update():
    """Update the site folder."""
    pull()
    pip()
    manage('syncdb')
    manage('migrate')
    manage('runserver')


@task
def deploy():
    update()
