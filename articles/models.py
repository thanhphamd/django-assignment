from django.db import models
from ckeditor.fields import RichTextField


class Article(models.Model):
    title = models.CharField(max_length=100)
    body = RichTextField(null=True)
    like = models.IntegerField(default=0)

    def __str__(self):              
        return self.title
